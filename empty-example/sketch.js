class Vert {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }
}

function setGrid(x, y, offset) {
let gameGrid = []; 
for (var i = 0; i < (x - offset); i+=offset) {
    for (var j = 0; j < (y - offset); j+=offset) {
      let tempX = i + Math.floor((Math.random()*offset));
      let tempY = j + Math.floor((Math.random()*offset));
      gameGrid.push(new Vert(tempX, tempY));
    }
  }
  return gameGrid;
}

let gameGrid = setGrid(600, 600, 150);
let dragging = false; // Is the object being dragged?
let rollover = false; // Is the mouse over the ellipse?
let offsetX, offsetY = 0;
let lastPoint = null;

function mousePressed() {
  let d = 5;
  /* // Did I click on the rectangle?
  if (mouseX > (lastPoint.x - d*5) && mouseX < (lastPoint.x + d*5) 
  && mouseY > (lastPoint.y - d*5) && mouseY < (lastPoint.y + d*5)) {
  } */
  const closestPoint = gameGrid.find(point => {
    const dist = Math.hypot(Math.abs(mouseX - point.x), Math.abs(mouseY - point.y))
    return dist < 5*d;
  });
  if (closestPoint) {
    lastPoint = closestPoint;
    dragging = true;
    // If so, keep track of relative location of click to corner of rectangle
    offsetX = closestPoint.x-mouseX;
    offsetY = closestPoint.y-mouseY;
  }
}

function mouseReleased() {
  // Quit dragging
  dragging = false;
}

function setup() {
  createCanvas(800, 800);
  console.log('setup');
}

function draw() {
  // put drawing code here
  background(255);
  let d = 5;
  strokeWeight(d);
  for (var i = 0; i < gameGrid.length; i++) {
    if (mouseX > (gameGrid[i].x - d*5) && mouseX < (gameGrid[i].x + d*5) 
    && mouseY > (gameGrid[i].y - d*5) && mouseY < (gameGrid[i].y + d*5)) {
      rollover = true;
    }
    else {
      rollover = false;
    }
    if (dragging) {
      lastPoint.x = mouseX + offsetX;
      lastPoint.y = mouseY + offsetY;
    }
    if (rollover) { stroke('pink'); }
    else { stroke('purple'); } // Change the color
    point(gameGrid[i].x, gameGrid[i].y);
  }
}

